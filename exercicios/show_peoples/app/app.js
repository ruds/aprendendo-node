"use strict";

var app = require('./configs/express/express');
var wsSocket = require('./sockets/wsServerPeople').peopleSocket;

var server = app.listen(3000,()=>console.log('Server running...'));

wsSocket(server);