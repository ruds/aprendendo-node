"use strict";

const PeopleController = require('./../controllers/PeopleController');
const WebSocketServer = require('ws').Server;

const controllers = new PeopleController();
var wsServer = undefined;

module.exports.peopleSocket = (server)=>{

    if(!wsServer)
        wsServer = new WebSocketServer({server, path: '/socket'})
    
    wsServer.on('connection',(ws)=>{

        emitMessage(ws);
    
        ws.on('message', (message, flags)=>{
            console.log(message);
        });
    
        ws.on('error', (error)=>{ 
            console.log(error.stack);
        });

        ws.on('generate', ()=>{
            let sendResponse = (error, data)=>{
                if (error) wsServer.emit('error',error);
                ws.send(data);
            };
            controllers.generateUsers(sendResponse);
        });
        
    });
    
    wsServer.on('error', (error)=>{
        console.log('Error!');
        console.log(error.stack);
        wsServer.close();
    });

}

var emitMessage = function (ws) { 

    console.log("Websockets clients connecteds: " + wsServer.clients.size);

    setInterval(()=>{
        ws.emit('generate');
    },6000);
};