"use strict";

var PeopleService = require('./../services/PeopleService').PeopleService;

module.exports = class PeopleController {
    
    constructor(){}

    generateUsers(callback){

        let dataPeople = (error, data)=>{
            if(error){
                console.log(error);
                return callback(new Error('Failed to receive data.'),null);
            }
            else 
                return callback(null,JSON.stringify(data));
        }

        PeopleService.createPeople(dataPeople);

    }
}