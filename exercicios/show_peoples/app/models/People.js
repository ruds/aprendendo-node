"use strict";

module.exports = class People {

    constructor(_gender, _nationality, _dateOfBirthy, _registred, _name, _location, _contact, _picture){
        this._gender = _gender;
        this._nationality = _nationality;
        this._dateOfBirthy = _dateOfBirthy;
        this._registred = _registred;
        this._name = _name;
        this._location = _location;
        this._contact = _contact;
        this._picture = _picture;
    }

    get gender(){
        return this._gender;
    }

    get nationality(){
        return this._nationality;
    }

    get dateOfBirthy(){
        return this._dateOfBirthy;
    }

    get registred(){
        return this._registred;
    }

    get name(){
        return this._name;
    }

    get location(){
        return this._location;
    }

    get contact(){
        return this._contact;
    }

    get picture(){
        return this._picture;
    }

}