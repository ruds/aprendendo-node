"use strict";
module.exports = class Location {

    constructor(_street, _city, _state, _postcode){
        this._street = _street;
        this._city = _city;
        this._state = _state;
        this._postcode = _postcode;
    }


    get street(){
        return this._street;
    }

    get city(){
        return this._city;
    }

    get state(){
        return this._state;
    }

    get postcode(){
        return this._postcode;
    }

}