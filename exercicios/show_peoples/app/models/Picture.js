"use strict";

module.exports = class Picture {

    constructor(_large, _medium, _thumbnail){
        this._large = _large;
        this._medium = _medium;
        this._thumbnail = _thumbnail;
    }

    get large(){
        return this._large;
    }

    get medium(){
        return this._medium;
    }

    get thumbnail(){
        return this._thumbnail;
    }
}
