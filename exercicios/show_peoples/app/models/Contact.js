"use strict";

module.exports = class Contact {

    constructor(_email, _phone, _cell){
        this._email = _email;
        this._phone = _phone;
        this._cell= _cell;
    }

    get email(){
        return this._email;
    }

    get phone(){
        return this._phone;
    }

    get cell(){
        return this._cell;
    }

}