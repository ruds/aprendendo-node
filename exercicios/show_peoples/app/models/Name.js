"use strict";

module.exports = class Name {

    constructor(_title, _first, _last){
        this._title = _title;
        this._first = _first;
        this._last = _last;
    }

    get title(){
        return this._title;
    }

    get first(){
        return this._first;
    }
    
    get last(){
        return this._last;
    }
}