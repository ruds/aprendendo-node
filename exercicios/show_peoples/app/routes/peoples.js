"use strict";

const router = require('express').Router();

router.get('/', function(req, res) {
    res.render('people/peoples');
});

router.get('/auto', function(req, res) {
    res.render('people/peoples_auto');
});

module.exports = router;