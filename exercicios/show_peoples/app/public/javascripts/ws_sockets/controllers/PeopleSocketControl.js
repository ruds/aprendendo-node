class PeopleSocketControl{

    constructor(_peopleView, _messageView){
        this._peopleView = _peopleView;
        this._messageView = _messageView;
    }

    addPeople(data){
        this._peopleView.update(data);
    }

    addInfoMessage(msg){
        this._messageView.update(msg,'info');
    }

    addErrorMessage(msg){
        this._messageView.update(msg,'error');
    }

    addWarnMessage(msg){
        this._messageView.update(msg,'warn');
    }

    removeMessageTimeout(){
        this._messageView.removeMessageTimeout(3000);
    }

}