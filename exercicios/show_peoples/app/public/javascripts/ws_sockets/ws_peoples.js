const socket = new WebSocket("ws://localhost:3000/socket");

var clientSocket = (controller)=>{

    socket.onopen = function () {
        console.log('Connection Established!');
        socket.send(JSON.stringify({id:undefined, data:"Hello! I'm a client!"}));
        controller.addInfoMessage('Connection Established!');
        controller.removeMessageTimeout(3000);
    };

    socket.onclose = function () {
        console.log('Connection Closed!');
        controller.addWarnMessage('Server connection lost.');
    };

    socket.onerror = function (error) {
        console.log(error);
    };

    socket.onmessage = function (e) {
        if (typeof e.data === "string") {
            controller.addPeople(JSON.parse(e.data));
        }
        else if (e.data instanceof ArrayBuffer) {
            console.log('ArrayBuffer received: ' + e.data);
        }
        else if (e.data instanceof Blob) {
            console.log('Blob received: ' + e.data);
        }
    };
};