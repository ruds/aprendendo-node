class MessageView{

    constructor(element){
        this._element = element;
        this._type = 'warn';
    }

    update(model, type){
        this._type = type;
        this._element.innerHTML = '';
        this._element.innerHTML += this._template(model);
    }

    removeMessageTimeout(timeout,type){
        console.log(timeout);
        setTimeout(() => {
            if(type)
                this._element.removeChild(this._element.querySelector(`h3.show-${this._type}`));
            else this._element.removeChild(this._element.querySelector(`h3.show-message`));
        }, timeout);
    }

    _template(model){
        return `
            <h3 class="show-${this._type} show-message">
                <p class="message">${model}</p>
            </h3>
        `;
    }
}