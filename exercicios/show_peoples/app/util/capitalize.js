"use strict";
/**
 * Capitalize module.
 * @module
 */

/**
 * Capitalize a string.
 * @param {string} string - string to capitalize.
 * @param {Array} excludes - ignoreds words.
 * @return {string} The capitalized string.
 */
module.exports = function capitalize(string, excludes = ['da','das', 'de','do','dos','e','o','a']){

    const exclude = element=>excludes.find(exclude=> exclude === element);

    if(string.includes(' '))
        return string.split(' ').map(element => 
            element = exclude(element) ? element : element.charAt(0).toUpperCase()+element.slice(1)).join(" ");
    else 
        return string.charAt(0).toUpperCase() + string.slice(1); 
}