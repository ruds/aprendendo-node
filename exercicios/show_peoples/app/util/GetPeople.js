"use strict";

var https = require('https');

module.exports =  class GetPeople{
    
    constructor(){}


    getData(url, path, callback){
    
        let data;
    
        let options = {
            hostname: url,
            path: path,
            method: 'GET',
        };
        
        let req = https.request(options, function (res) {  
            
            res.setEncoding('utf8');
        
            res.on('data', function (chunck) {
                if(!data) data = chunck;
                else data += chunck;
            });
        
            res.on('end', function () { 
                let obj = JSON.parse(data);
                return callback(null, obj.results[0]);
            });
        });
        
        req.on('error', function (e) {  
            console.log('An error as ocurred: ' + e.message);
            console.log('Stack trace: ' + e.stack);
            return callback(e, null);
        });
    
        req.end();
    }
}