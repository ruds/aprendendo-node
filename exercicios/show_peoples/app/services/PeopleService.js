"use strict";

var GetPeople = require('./../util/GetPeople');
var url = 'randomuser.me'

var Contact = require('./../models/Contact');
var Location = require('./../models/Location');
var Name = require('./../models/Name');
var Picture = require('./../models/Picture');
var People = require('./../models/People');
var capitalize = require('./../util/capitalize');

module.exports.PeopleService = class PeopleService {
    static createPeople(callback) {

        let path = '/api/?nat=br&exc=login,id,info';

        let makePeople = (error, data)=>{

            if(error){
                console.log(error);
                return callback(error, null);
            }
            else{
                let contact = new Contact(data.email, data.phone, data.cell);
                let location = new Location(capitalize(data.location.street), capitalize(data.location.city), capitalize(data.location.state));
                let name = new Name(capitalize(data.name.title), capitalize(data.name.first), capitalize(data.name.last));
                let picture = new Picture(data.picture.large, data.picture.medium, data.picture.thumbnail);
                let people = new People(capitalize(data.gender), data.nat, data.dob, data.registered, name, location, contact, picture);
                return callback(null, people);
            }
        }

        let getPeople = new GetPeople();

        getPeople.getData(url, path, makePeople);
    }
}